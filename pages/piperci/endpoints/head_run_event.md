---
title: HEAD /run/{run_id}/events
sidebar: mydoc_sidebar
permalink: head_run_event.html
folder: piperci
swaggerfile: swagger
swaggerkey: /run/{run_id}/events
method: head
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_thread_header.html" model= "Thread Header" %}
