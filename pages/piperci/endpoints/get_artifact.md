---
title: GET /artifact/{artifact}
sidebar: mydoc_sidebar
permalink: /get_artifact.html
folder: piperci
swaggerfile: swagger
swaggerkey: /artifact/{artifact}
method: get
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_artifact.html" model= "Artifact" %}
