---
title: POST /task
sidebar: mydoc_sidebar
permalink: task.html
folder: piperci
swaggerfile: swagger
swaggerkey: /task
method: post
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}

## Body parameters
{% include swagger_parser/getrequestbody.md %}

## Examples
<pre>
<code>Task Creation

{
    "run_id": "1",
    "project": "test suite",
    "caller": "test_case_1",
    "message": "test creation of a normal task",
    "status": "started"
}
</code>
</pre>

<pre>
<code>Thread Task Creation

{
    "run_id": "2",
    "project": "test suite",
    "caller": "test_case_2",
    "message": "test creation of a thread",
    "status": "received",
    "thread_id": "f1b66f41-321c-401c-baff-30c9239b3285",
    "parent_id": "f1b66f41-321c-401c-baff-30c9239b3285"
}
</code>
</pre>

## Responses
{% include swagger_parser/getresponses.md path="/api_task_event.html" model= "Task Event" %}
