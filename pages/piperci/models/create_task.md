---
title: Create Task
sidebar: mydoc_sidebar
permalink: api_create_task.html
folder: piperci
swaggerfile: swagger
swaggerkey: CreateTask
---
## Create task attributes
{% include swagger_parser/getmodel.md required="yes"  %}

{% include links.html %}
