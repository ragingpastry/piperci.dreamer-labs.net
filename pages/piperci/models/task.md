---
title: Task
sidebar: mydoc_sidebar
permalink: api_task.html
folder: piperci
swaggerfile: swagger
swaggerkey: Task
---
## Task attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}
