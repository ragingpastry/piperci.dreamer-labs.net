---
title: Artifact Header
sidebar: mydoc_sidebar
permalink: api_artifact_header.html
folder: piperci
swaggerfile: swagger
swaggerkey: artifact_header
---
## Header attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}
