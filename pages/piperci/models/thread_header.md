---
title: Thread Header
sidebar: mydoc_sidebar
permalink: api_thread_header.html
folder: piperci
swaggerfile: swagger
swaggerkey: thread_header
---
## Header attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}
