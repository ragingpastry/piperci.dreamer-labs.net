---
title: Task Header
sidebar: mydoc_sidebar
permalink: api_task_header.html
folder: piperci
swaggerfile: swagger
swaggerkey: task_header
---
## Header attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}
