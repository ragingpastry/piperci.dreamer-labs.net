{% assign attribute = site.data.swagger[page.swaggerfile]paths[page.swaggerkey][page.method]requestBody.content %}
{% assign ref = "$ref" %}

{% for value in attribute %}
  <table>
    <thead>
    <tr>
        <th>Name</th><th>Type</th><th>Description</th><th>Required?</th>
    </tr>
    </thead>

      {% if value[1].schema.type == "array" %}
          {% assign request = value.[1]schema.items[ref] | remove_first:'#/components/schemas/' %}
      {% else %}
          {% assign request = value[1].schema[ref] | remove_first:'#/components/schemas/' %}
      {% endif %}

      <!-- Read the "required" array from Swagger for use later -->
      {% assign required = site.data.swagger[page.swaggerfile]components.schemas[request]required %}
      <tbody>
        {% for param in site.data.swagger[page.swaggerfile]components.schemas[request]properties %}
          {% if param[0] == 'id' %}
          {% else %}
            <tr>
              <td><code>{{ param[0] }}</code></td>
              <td>
              {% if param[1].[ref] %}
                <code>object&#60;{{ param[1].[ref] | remove_first:'#/components/schemas/' }}&#62;</code>
              {% else %}
                <code>{{ param[1].type }}</code>
                  {% if param[1].format %}
                  <code>&#60;{{ param[1].format }}&#62;</code>
                  {% endif %}
              {% endif %}
              </td>
              <td>
                <!-- If it's an enum, list the values before the description -->
                {% if param[1].enum %}
                Values: <br>
                    {% for value in param[1].enum %}
                      <code>{{ value }}</code>,<br>
                    {% endfor %}
                {% endif %}
                {{ param[1].description }}
              </td>
              <td>
                {% if property[1].readOnly == true %}
                  READ<br>ONLY
                {% endif %}
              <!-- Get the "required" attributes from the Swagger file -->
                {% for reqprop in required %}
                    {% if reqprop == param[0] %}
                      REQUIRED
                    {% endif %}
                {% endfor %}
              </td>
            </tr>
          {% endif %}
        {% endfor %}
    </tbody>    
  </table>

{% endfor %}
